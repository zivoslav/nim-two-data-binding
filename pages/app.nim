
import std/dom
import std/tables
# import std/strtabs
import std/strformat

echo "ja jsem skript z nimu zrozený"

# const state = {
#   name: 'Francesco',
#   title: 'Front-end Developer'
# };

type State = ref object of RootObj
  # dataset: StringTableRef
  dataset: Table[cstring, cstring]
  route: cstring


type KEYS = enum

  NAME = "name"
  TITLE = "title"

proc newState(): State =
  return State(
    # dataset: newStringTable(),
    dataset: initTable[cstring, cstring](),
    route: "/"
  )

proc render()

proc `[]=`(state: State,  key: cstring, value: cstring) =
  
  state.dataset[key] = value

  render()
  
proc `[]`(state: State,  key: cstring): cstring =

  return state.dataset[key]


var state = newState()
state[$NAME] = "Francesco"
state[$TITLE] = "Front-end Developer"

# ---------------------------------------------------------------

# import './style.css';

# const createState = (state) => {
#   return new Proxy(state, {
#     set(target, property, value) {
#       target[property] = value;
#       render();
#       return true;
#     }
#   });
# };


# const listeners = document.querySelectorAll('[data-model]');

# listeners.forEach((listener) => {
#   const name = listener.dataset.model;
#   listener.addEventListener('keyup', (event) => {
#     state[name] = listener.value;
#     console.log(state);
#   });
# });

proc bindModel(ev: Event) =

  let listener = ev.target

  let key = listener.getAttribute "data-model" 

  echo "key ", key
  echo "state ", $state.dataset

  state[key] = ev.target.value

proc init() =

  for listener in document.querySelectorAll("[data-model]"):

    let name = listener.getAttribute "data-model" #.dataset #.model.name

    echo name

    listener.addEventListener($KeyUp, bindModel)




# const render = () => {
#   const bindings = Array.from(document.querySelectorAll('[data-binding]')).map(
#     e => e.dataset.binding
#   );
#   bindings.forEach((binding) => {
#     document.querySelector(`[data-binding='${binding}']`).innerHTML = state[binding];
#     document.querySelector(`[data-model='${binding}']`).value = state[binding];
#   });
# };

# render();

proc render() =

  echo "render bude zde"

  for element in document.querySelectorAll("[data-binding]"):

    let binding = element.getAttribute "data-binding"

    echo "binding ", binding

    echo &"[data-binding='${binding}'"

    document.querySelector(cstring &"[data-binding='{binding}']").innerHTML = state[binding]
    document.querySelector(cstring &"[data-model='{binding}']").value = state[binding]

    # echo element

# -----------------------------------------------------------------------------------------------------

# proc dataset(element: Element) {.importc: "dataset".}

proc onLoad(ev: Event) =

  window.alert "tož jede to nebo ne"

  init()

  # let state: State = (name: cstring "Francesco", title: cstring "Front-end Developer")

  document.querySelector("[data-binding=\"name\"]").innerHTML = state[$NAME]
  document.querySelector("[data-binding=\"title\"]").innerHTML = state[$TITLE]
  document.querySelector("[data-model=\"name\"]").value = state[$NAME]
  document.querySelector("[data-model=\"title\"]").value = state[$TITLE]

  state[$NAME] = "Richard"
  state[$TITLE] = "Technical Lead"

  render()

window.addEventListener($Load, onLoad)

